# StatsFile

**Retrieves statistics from files or folders**.


* **Last Access**  => The date of the last access. 
* **Last Modified**  => The last modification date.
* **Date of creation** => Be careful, most of the time, you will find the same dates as the last modification.
* **GID** (Group Identifier) => https://en.wikipedia.org/wiki/Group_identifier
* **UID** (User Identifier) => https://en.wikipedia.org/wiki/User_identifier
* **Size** => Size in bytes
* **Mode** (**Chmod**) => Displays file permissions (read, write, execute)
* **Owner** => To whom this file / directory belongs ?
* **Group Member** => According to the GID, displays who else can read, write, execute 

## Terminal

### **Linux** & **MacOS**

<pre>
    # Example  
    python3 script.py -p "/myfolder/"
    python3 script.py -p "/myfolder/myfile.txt"
    
    # System Example
    python3 script.py -p "/etc"
    
    python3 script.py -p "/Users/theUsername/Desktop/" # MacOS
    python3 script.py -p "/Users/theUsername/Desktop/myfile.txt" # MacOS
    
    python3 script.py -p "/Home/theUsername/Desktop/" # Linux
    python3 script.py -p "/Home/theUsername/Desktop/myfile.pdf" # Linux
</pre>

## ToDo
* [ ] Retrieve metadatas from files


## Contact

Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered 

* **Twitter :** https://twitter.com/mxmmoreau
* **LinkedIn :** https://www.linkedin.com/in/mxmoreau/
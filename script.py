import os.path
import stat
import argparse
import pwd
import grp
import getpass
import platform as plf 
from datetime import datetime

def chmod(data):

    if isinstance(data, str):

        chmod = {
            "-": "File",
            "d": "Directory",
            "l": "Link",
            "---": 0, # No rights
            "--x": 1, # Execution only
            "-w-": 2, # Writing only
            "-wx": 3, # Writing & execution
            "r--": 4, # Read only
            "r-x": 5, # Reading & execution
            "rw-": 6, # Reading & Writing
            "rwx": 7, # All rights
        }

        data = data[:3]

        if data[-1] == "t":
            data = data[:2] + "x"

        if data[:3] in chmod.keys():
            return chmod[data]


try:
    if plf.system() in ("Linux", "Darwin"):

        parser = argparse.ArgumentParser()
        parser.add_argument("-p", "--path", type=str)
        args = parser.parse_args()

        path = args.path 

        if path:
            print("Script Launched On :", datetime.now())
            print("Script Executed By :", getpass.getuser())
            print(">", path, "\n")

            if not os.path.exists(path):
                print(".:: File or Directory not existing ! ::.\n")
            else:
                print("os.path")

                print("\tLast Access        :", os.path.getatime(path), "=>", datetime.fromtimestamp(os.path.getatime(path)))
                print("\tLast Modified      :", os.path.getmtime(path), "=>", datetime.fromtimestamp(os.path.getmtime(path)))
                print("\tCreated            :", os.path.getctime(path), "=>", datetime.fromtimestamp(os.path.getctime(path)), "\n")

                # --------------------------------------------------------------------------------------------------------

                print("os.stat")

                statfile = os.stat(path)

                print("\tLast Access        :", statfile.st_atime, "=>", datetime.fromtimestamp(statfile.st_atime))
                print("\tLast Modified      :", statfile.st_mtime, "=>", datetime.fromtimestamp(statfile.st_mtime))
                print("\tCreated            :", statfile.st_ctime, "=>", datetime.fromtimestamp(statfile.st_ctime), "\n")
                
                gid = statfile.st_gid
                uid = statfile.st_uid

                print("\tUSER               :", pwd.getpwuid(uid).pw_name)
                print("\tGID                :", gid, "=>", grp.getgrgid(gid).gr_name)
                print("\tUID                :", uid, "\n")
                
                print("\tGroup Member       :")
                [print("\t\t\t    ", grp) for grp in grp.getgrgid(gid).gr_mem]

                print("\n\tSize               :", statfile.st_size, "bytes")

                mode = stat.filemode(statfile.st_mode)
                mode_type = mode[0]
                mode_owner = mode[1:4]
                mode_group = mode[4:7]
                mode_other = mode[7:10]
                
                print("\tMode               :", mode, "=>", chmod(mode_type), str(chmod(mode_owner)) + str(chmod(mode_group)) + str(chmod(mode_other)))
                print("\t    Type           :", mode_type, "=>", chmod(mode_type))
                print("\t    Owner          :", mode_owner, "=>", chmod(mode_owner))
                print("\t    Group          :", mode_group, "=>", chmod(mode_group))
                print("\t    Other          :", mode_other, "=>", chmod(mode_other), "\n")
                
                # --------------------------------------------------------------------------------------------------------

                print("os.access")

                read = os.access(path, os.R_OK)
                write = os.access(path, os.W_OK)
                execution = os.access(path, os.X_OK)

                mode = (["-", "r"][read] + ["-", "w"][write] + ["-", "x"][execution])

                print("\tRead      :", read)
                print("\tWrite     :", write)
                print("\tExecution :", execution, "\n")

                print("\tMode      :", mode, "=>", chmod(mode), "\n")
except:
    pass